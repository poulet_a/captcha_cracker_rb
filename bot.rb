#!/usr/bin/env ruby
#encoding: utf-8

require 'yaml'
require 'pry'
require 'uri'
require 'rtesseract'
require 'mechanize'
require 'colorize'

class BotCaptcha

  def initialize url, user, pass
    @agent = Mechanize.new
    @agent.basic_auth(user, pass)
    @agent.get(url)
    @url = url
  end

  SAVE_PATH = "/tmp/captcha.png"
  # returns String
  def resolv_img
    `rm -f #{SAVE_PATH}`
    @image.save_as(SAVE_PATH)
    s = RTesseract.new(SAVE_PATH).to_s.strip
    return s
  end

  def get_img
    url = URI(CONFIG['url']) + 'captcha.php'
    @agent.get @agent.get(url.to_s)
    @agent.page
  end

  def valid_captcha pass
    @agent.post(CONFIG['url'], {'password' => pass})
  end

  def resolv_n n = 1000
    @n = 0
    @i = 0
    @x = 0
    @t = Time.now
    while (@i = @agent.page.search('p').last.text.split('/').first.split.last.to_i) < n
      puts "#{@i}/#{n}"
      @x = @i if @x < @i
      @n = @n + 1
      @image = get_img
      pass = resolv_img
      valid_captcha pass
      puts "tried : #{pass}, #{(@i / @n.to_f * 100).round 1}%, #{(Time.now - @t).to_i} seconds"
    end
  end

end

if __FILE__ == $0
  CONFIG = YAML.load_file('config.yml')
  begin
    b = BotCaptcha.new CONFIG['url'], CONFIG['user'], CONFIG['pass']
    b.resolv_n
  rescue => e
    puts e.message.to_s.red
    puts "Failed... retry in 5 seconds".red
    sleep 5
    retry
  end
end
